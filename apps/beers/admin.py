from django.contrib import admin
from .models import Beer, BeerGenre


admin.site.register(Beer)
admin.site.register(BeerGenre)
