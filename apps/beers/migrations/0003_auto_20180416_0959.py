# Generated by Django 2.0.4 on 2018-04-16 09:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('beers', '0002_auto_20180416_0956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='beergenre',
            name='description',
            field=models.TextField(blank=True, max_length=1000, null=True),
        ),
    ]
