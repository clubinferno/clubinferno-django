from django.contrib import admin
from .models import Sponsor, SponsorProvider


admin.site.register(Sponsor)
admin.site.register(SponsorProvider)
