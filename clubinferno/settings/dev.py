# clubinferno/settings/dev.py
# -*- coding: UTF-8 -*-
from .base import *
import os

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*7qmxy-(v$iodv&k8v^f@4^k@qlnaz6utau)(ztuzpt_pv*1j('

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'clubinferno',
        'USER': 'root',
        'PASSWORD': 'clubinferno',
        'HOST': 'postgres',
        'PORT': '5432'
    }
}
